\select@language {czech}
\contentsline {section}{\numberline {1}Z\IeC {\'a}kladn\IeC {\'\i } popis hry}{3}
\contentsline {section}{\numberline {2}Popis protokolu}{4}
\contentsline {subsection}{\numberline {2.1}Klient}{4}
\contentsline {subsection}{\numberline {2.2}Server}{6}
\contentsline {section}{\numberline {3}Implementace}{7}
\contentsline {subsection}{\numberline {3.1}Klient}{7}
\contentsline {subsubsection}{\numberline {3.1.1}Rozvrstevn\IeC {\'\i } aplikace}{7}
\contentsline {subsubsection}{\numberline {3.1.2}Chybov\IeC {\'e} stavy}{8}
\contentsline {subsection}{\numberline {3.2}Server}{8}
\contentsline {subsubsection}{\numberline {3.2.1}S\IeC {\'\i }\IeC {\v t}ov\IeC {\'e} spojen\IeC {\'\i }}{8}
\contentsline {subsubsection}{\numberline {3.2.2}Obsluha hry}{8}
\contentsline {subsubsection}{\numberline {3.2.3}Obsluha p\IeC {\v r}\IeC {\'\i }choz\IeC {\'\i }ch zpr\IeC {\'a}v}{8}
\contentsline {subsubsection}{\numberline {3.2.4}Datov\IeC {\'e} struktury}{8}
\contentsline {section}{\numberline {4}U\IeC {\v z}ivatelsk\IeC {\'a} dokumentace}{10}
\contentsline {subsection}{\numberline {4.1}Server}{10}
\contentsline {subsubsection}{\numberline {4.1.1}P\IeC {\v r}eklad a spu\IeC {\v s}t\IeC {\v e}n\IeC {\'\i } serveru}{10}
\contentsline {subsubsection}{\numberline {4.1.2}Logov\IeC {\'a}n\IeC {\'\i }}{11}
\contentsline {subsection}{\numberline {4.2}Klient}{11}
\contentsline {subsubsection}{\numberline {4.2.1}P\IeC {\v r}eklad klienta v syst\IeC {\'e}mu Linux}{11}
\contentsline {subsubsection}{\numberline {4.2.2}P\IeC {\v r}eklad klienta v syst\IeC {\'e}mu Windows}{11}
\contentsline {subsubsection}{\numberline {4.2.3}Ovl\IeC {\'a}d\IeC {\'a}n\IeC {\'\i } a popis klienta}{11}
\contentsline {section}{\numberline {5}Hardwarov\IeC {\'e} n\IeC {\'a}roky}{15}
\contentsline {subsection}{\numberline {5.1}Klient}{15}
\contentsline {subsection}{\numberline {5.2}Server}{15}
\contentsline {section}{\numberline {6}P\IeC {\v r}\IeC {\'\i }klad komunikace klient-server}{16}
\contentsline {subsection}{\numberline {6.1}Zalo\IeC {\v z}en\IeC {\'\i } p\IeC {\v r}ipojen\IeC {\'\i } do hern\IeC {\'\i } m\IeC {\'\i }stnosti, \IeC {\'u}sp\IeC {\v e}\IeC {\v s}n\IeC {\'e} dohr\IeC {\'a}n\IeC {\'\i }}{16}
\contentsline {subsection}{\numberline {6.2}N\IeC {\'a}vrat do rozehran\IeC {\'e} hry}{17}
\contentsline {section}{\numberline {7}Z\IeC {\'a}v\IeC {\v e}r}{19}
