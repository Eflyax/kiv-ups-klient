/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package soc.Classes;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import soc.constants;

/**
 *
 * @author eflyax
 */
public class Attack {

    private Image texture;
    constants.Category category;
    int direction;
    
    
    public Attack(constants.Category category) {
	this.category = category;
	
    }
    
     public void draw(GraphicsContext gc) {
	
	gc.drawImage(this.texture, 10,10);
	
    }
    
    
}
