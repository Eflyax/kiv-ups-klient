/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package soc.Classes;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import soc.constants;

/**
 *
 * @author Eflyax
 */
public class Castle {

    private int positionX = 0;
    private int positionY = 0;
    private final int radius = 160;
    private int maxHealth = 500;
    private int health;
    private int healthBarWidth = 300;
    private Side side;

    public enum Side {
	LEFT, RIGHT
    };

    private Image image;

    public Castle(Side s, int team) {
	this.health = this.maxHealth;
	if (s == Side.LEFT) {
	    this.side = Side.LEFT;
	} else {
	    this.side = Side.RIGHT;
	}
	this.image = new Image(getClass().getResource("/soc/images/sprite/" + Integer.toString(team) + "/castle.png").toExternalForm());
    }

    public void draw(GraphicsContext gc) {
	gc.fillRoundRect(positionX - radius, positionY, radius * 2, constants.CANVAS_HEIGHT - positionY + 50, radius, radius);
	if (this.side == Side.LEFT) {
	    gc.drawImage(this.image, positionX - radius / 2, positionY - (image.getHeight()) + 5, (radius), image.getHeight());
	} else {
	    gc.drawImage(this.image, positionX - radius / 2 + radius, positionY - (image.getHeight()) + 5, -(radius), image.getHeight());
	}
    }

    public void drawHealthBar(GraphicsContext gc) {

	double onePr = (double) ((double) this.healthBarWidth / (double) this.maxHealth);
	if (this.side == Side.LEFT) {
	    gc.setFill(Color.GRAY);
	    gc.fillRect(10, 10, healthBarWidth + 10, 25);
	    gc.setFill(Color.GREEN);
	    gc.fillRect(15, 15, this.health * onePr, 15);
	    gc.strokeText(this.health + "/" + this.maxHealth, 10, 50);
	} else {
	    gc.setFill(Color.GRAY);
	    gc.fillRect(constants.CANVAS_HEIGHT + healthBarWidth - 10, 10, healthBarWidth + 10, 25);
	    gc.setFill(Color.GREEN);
	    gc.fillRect(constants.CANVAS_HEIGHT + healthBarWidth - 5, 15, this.health * onePr, 15);
	    gc.strokeText(this.health + "/" + this.maxHealth, constants.CANVAS_HEIGHT + healthBarWidth - 10, 50);
	}
    }

    public int setDamage(int value) {
	this.health -= value;

	if (this.health <= 0) {
	    this.health = 0;
	    return 0;
	} else {
	    return 1;
	}
    }

    public void setPositionX(int positionX) {
	this.positionX = positionX;
    }

    public void setPositionY(int positionY) {
	this.positionY = positionY;
    }

    public int getRadius() {
	return radius;
    }

    public int getPositionX() {
	return positionX;
    }

    public int getPositionY() {
	return positionY;
    }

    public int getHealth() {
	return health;
    }

    public void setHealth(int value) {
	this.health = value;
    }

}
