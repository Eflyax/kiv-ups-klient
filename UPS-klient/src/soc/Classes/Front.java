package soc.Classes;

import java.util.List;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

/**
 *
 * @author eflyax
 */
public class Front {

    Warrior[] frontWarriors;
    int pointer;
    int size;

    int xDraw, yDraw;
    int widthDraw = 150, heightDraw;

    public Front(int size, int xDraw, int yDraw) {
        this.frontWarriors = new Warrior[size];
        this.pointer = -1;
        this.size = size;
        this.xDraw = xDraw - this.widthDraw / 2;
        this.yDraw = yDraw;
        this.heightDraw = widthDraw / this.size;
    }

    public boolean addWarrior(Warrior warrior) {

        if (pointer + 1 >= this.size) {
            return false;
        }

        this.frontWarriors[++pointer] = warrior;
        return true;
    }

    public boolean update(List<Warrior> listOfWarriors) {

        if (this.frontWarriors[0] != null) {
            Warrior w = this.frontWarriors[0];
            w.lowerTrainingTime();
            if (w.getTrainingTime() <= 0) {
                // pripraven k vypusteni
                if (!spawnCollision(listOfWarriors)) {
                    listOfWarriors.add(w);
                    this.shiftFront();
		    return true;
                }
            }
        }
	return false;
    }

    public boolean spawnCollision(List<Warrior> listOfWarriors) {

        if (listOfWarriors.size() > 0) {
            int direction = listOfWarriors.get(0).getDirection();
            int indexWar = 0;
            if (direction == 1) {
                int max = 1000;
                for (int i = 0; i < listOfWarriors.size(); i++) {
                    if (listOfWarriors.get(i).getPositionX() < max) {
                        max = listOfWarriors.get(i).getPositionX();
                        indexWar = i;
                    }
                }
                if ((this.frontWarriors[0].getPositionX() + this.frontWarriors[0].getRadius()) < (listOfWarriors.get(indexWar).getPositionX() + listOfWarriors.get(indexWar).getRadius())) {
                    return false;
                } else {
                    return true;
                }
            } else {
                int max = 0;
                for (int i = 0; i < listOfWarriors.size(); i++) {
                    if (listOfWarriors.get(i).getPositionX() > max) {
                        max = listOfWarriors.get(i).getPositionX();
                        indexWar = i;
                    }
                }
                if ((this.frontWarriors[0].getPositionX() + this.frontWarriors[0].getRadius()) > (listOfWarriors.get(indexWar).getPositionX() + listOfWarriors.get(indexWar).getRadius())) {
                    return false;
                } else {
                    return true;
                }
            }
        }
        return false;
    }

    public void shiftFront() {
        try {
            for (int i = 0; i < this.frontWarriors.length - 1; i++) {
                frontWarriors[i] = frontWarriors[i + 1];
            }
        } catch (Exception e) {
            System.out.println("Chyba při posunu fronty");
        }
        frontWarriors[this.frontWarriors.length - 1] = null;
        pointer--;
    }

    public void draw(GraphicsContext gc) {

        gc.setFill(Color.BURLYWOOD);
        gc.fillRoundRect(xDraw, yDraw, heightDraw * this.size, heightDraw, 10, 10);

        for (int i = 0; i < this.size; i++) {
            gc.setFill(Color.BLACK);
            if (this.frontWarriors[i] != null) {
                if (i == 0) {
                    gc.setFill(Color.BEIGE);
                    gc.fillRect(xDraw + i * heightDraw, yDraw, heightDraw, heightDraw);
                    gc.setFill(Color.RED);
                    double pr = (double) this.widthDraw / (double) this.frontWarriors[i].getTrainingTimeDefault();
                    gc.fillRect(xDraw, yDraw + heightDraw, this.frontWarriors[i].getTrainingTime() * pr + 1, heightDraw / 4);
                }
                gc.setFill(Color.BLACK);
                gc.strokeText(this.frontWarriors[i].getCategory().toString(), xDraw + i * heightDraw + heightDraw / 2, yDraw + heightDraw / 2);
            }
        }
    }
    
    public String getFrontToString(){
	System.out.println("jsem ve front to string");
	
        if (this.frontWarriors[0] != null) { 
	    System.out.println("ma vojaky");
        
            StringBuilder sb = new StringBuilder();
            for(int i = 0; i < this.frontWarriors.length; i++) {
		if(this.frontWarriors[i] == null){
		    break;
		}
                sb.append(this.frontWarriors[i].getCategory().toString()).append("#");
            }
            sb.append("&").append(this.frontWarriors[0].getTrainingTime()).append(":");
            return sb.toString();
        }else{
	    System.out.println("nema vojaky");
            return "x:";
        }	
    }
}
