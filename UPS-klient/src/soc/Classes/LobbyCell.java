/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package soc.Classes;

/**
 *
 * @author eflyax
 */
public class LobbyCell {

    private String leftName;
    private String rightName;
    private String imgL;
    private int idRoom;
    private boolean full;

    public LobbyCell(String leftName, String rightName, int idRoom) {
	this.leftName = leftName;
	this.rightName = rightName;
	this.imgL = "game.png";
	this.idRoom = idRoom;
	this.full = true;
	if(rightName.equals("\t\t")){
	    this.full = false;
	}
    }

    public String getImgL() {
	return this.imgL;
    }

    public int getIdRoom() {
	return this.idRoom;
    }

    public String getLeftName() {
	return leftName;
    }

    public String getRightName() {
	return rightName;
    }

    public boolean isFull() {
	return full;
    }

}
