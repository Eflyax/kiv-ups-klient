/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package soc.Classes;

import java.util.Random;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.ImagePattern;
import soc.constants;

/**
 *
 * @author Eflyax
 */

public class Map {

    private int[] terrain;
    private static Random rnd = new Random();
    private int ofs = 0;
    private int max = 0;
    /* vertikální posun mapy */
    private final int shift = 400;
    private int padding = 100;
    
    private int amplSin, amplCos, noise, sin, cos, width;
    private String seed;   
	
	private Image grass_down_01;
	private Image grass_down_02;
	
	private Image dirt;
	
    
    public Map() {
        this(constants.MAP_WIDTH, 1, 2,  10, 10, 0);
    }
    
    public Map(int[] a) {
        this(constants.MAP_WIDTH, a[0], a[1], a[2], a[3], a[4]);
    }
    
    public Map (int width, int sinuses, int cosinuses, int amplsin, int amplcos, int noise) {     
        this.width = width;
        this.sin = sinuses;
        this.cos = cosinuses;
        this.amplSin = amplsin;
        this.amplCos = amplcos;
        this.noise = noise; 
        this.modelTerrain(this.width, this.sin, this.cos, this.amplSin, this.amplCos, this.noise);
		initSources();
    }
	
	private void initSources() {
		this.dirt = new Image(getClass().getResource("/soc/images/terrain/dirt.jpg").toExternalForm()); 
		this.grass_down_01 = new Image(getClass().getResource("/soc/images/terrain/grass-down-01.png").toExternalForm()); 
		this.grass_down_02 = new Image(getClass().getResource("/soc/images/terrain/grass-down-02.png").toExternalForm()); 		 
	}
    
    public void modelTerrain(int[] a) {
        modelTerrain(constants.MAP_WIDTH, a[0], a[1], a[2], a[3], a[4]);
    }
	
    public void modelTerrain(int width, int sinuses, int cosinuses, int amplsin, int amplcos, int noise){   
        this.width = width;
        this.sin = sinuses;
        this.cos = cosinuses;
        this.amplSin = amplsin;
        this.amplCos = amplcos;
        this.noise = noise;
        this.ofs = 0;
        this.max = 0;
		
        int[] generatedTerrain = new int[(int)this.width];
        for(int i = 0; i< generatedTerrain.length; i++) {
            generatedTerrain[i] = 0;
        }
        
        for (int i = 0; i < this.width; i++) {
            for (int sinC = 1; sinC <= this.sin; sinC++) {
                generatedTerrain[i] += Math.round(this.amplSin * Math.sin((2 * sinC * i * Math.PI) / (double)width));
            }
            for (int cosC = 1; cosC <= this.cos; cosC++) {
                generatedTerrain[i] += Math.round(this.amplCos * Math.cos((2 * cosC * i * Math.PI) / (double)width));
            }
            generatedTerrain[i] += Math.round((this.noise * this.amplCos ) - (this.noise * this.amplSin ));
            if(generatedTerrain[i] < ofs){
               ofs = generatedTerrain[i];
            } 
            if(generatedTerrain[i] > max){
               max = generatedTerrain[i];
            }
        }
     
        if (ofs < 0) {
            ofs *= -1;
            for (int i = 0; i < width; i++) {
                generatedTerrain[i] += ofs;
            }
        }

        double space = (shift - max);
        for (int i = 0; i < this.width; i++) {
            generatedTerrain[i] += space;
        }

        int newMax = 0;
        for(int i = 0; i< generatedTerrain.length; i++) {
            if(generatedTerrain[i] > newMax)
                newMax = generatedTerrain[i];
        }
        this.terrain = generatedTerrain;
    }
    
    public void draw(GraphicsContext gc) {
        //Path cesta = new Path();
        gc.beginPath();
        gc.moveTo(0,this.terrain[0]);
		int terrainValue;	   
		
        for(int i = 0;  i< this.terrain.length;i++){
            terrainValue = this.terrain[i];
            gc.lineTo(i,terrainValue);
        }
		 
        gc.lineTo(gc.getCanvas().getWidth(),gc.getCanvas().getHeight());
        gc.lineTo(0,gc.getCanvas().getHeight());
        gc.lineTo(0,this.terrain[0]);
        gc.setFill(new ImagePattern(dirt, 0, 0, 100, 100, false));
		gc.closePath();
        gc.stroke();
        gc.fill();
		
		for(int i = 0;  i< this.terrain.length;i++){
			terrainValue = this.terrain[i];
			gc.drawImage(this.grass_down_01, i-grass_down_01.getWidth(), terrainValue, grass_down_01.getWidth(), grass_down_01.getHeight());
			i += grass_down_01.getWidth();
        }
    }
    
    public void setCastles(Castle left, Castle right, GraphicsContext gc){
        left.setPositionX(this.padding);
        left.setPositionY(this.terrain[left.getPositionX()+left.getRadius()]);
        right.setPositionX(this.width-this.padding);
        right.setPositionY(this.terrain[right.getPositionX()-right.getRadius()]);
        for(int i = 0; i < left.getPositionX()+left.getRadius();i++){
            terrain[i] = left.getPositionY();
        }
        for(int i = right.getPositionX()-right.getRadius(); i<terrain.length;i++){
            terrain[i] = right.getPositionY();
        }
		
		
		for(int i = 0;  i< this.terrain.length;i++){
			int terrainValue = this.terrain[i];

			gc.drawImage(this.grass_down_01, i-grass_down_01.getWidth(), terrainValue-grass_down_01.getHeight()/2, grass_down_01.getWidth(), grass_down_01.getHeight());
			i += grass_down_01.getWidth();
		}

    }
    
    public void clearCanvas(GraphicsContext gc) {
        gc.clearRect(0, 0, constants.STAGE_WIDTH, constants.STAGE_HEIGHT);
    }
    
    private int findMaxTerrain(int start, int end){
        int GlobalMax = this.terrain[start];
        for(int i = start; i < end; i++){
            
            if(GlobalMax > this.terrain[i]){
                GlobalMax = this.terrain[i];
            }
        }        
        return GlobalMax;
    }
    
    @Override
    public String toString() {
        return "Sin:"+this.getSin()+", Cos:"+this.getCos()+", AmplSin:"+this.getAmplSin()+", AmplCos:"+this.getAmplCos()+", velikost:"+this.width+", Noise: "+this.getNoise();
    }
  
    public int[] getTerrain() {
        return terrain;
    }
    
    public int getSin() {
        return this.sin;
    }

    public int getMax() {
        return max;
    }

    public int getShift() {
        return shift;
    }

    public int getCos() {
        return cos;
    }

    public int getWidth() {
        return width;
    }

    public int getAmplSin() {
        return amplSin;
    }

    public int getAmplCos() {
        return amplCos;
    }

    public int getNoise() {
        return noise;
    }

    public String getSeed() {
        return seed;
    }
    
    public int getTerrainY(int x){
        return this.terrain[x];
    }
}
