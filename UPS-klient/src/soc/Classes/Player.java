/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package soc.Classes;

import java.util.ArrayList;
import java.util.List;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import soc.constants;

/**
 *
 * @author Eflyax
 */
public class Player {

    private String name;
    private Castle myCastle;
    private Castle oponCastle;
    List<Warrior> listOfWarriors = new ArrayList<>();
    private int rank;
    private int direction;
    private Map map;
    private Color color;
    private Front frontWar;
    private int frontSize = 5;
    private int team;
    private int money;

    public Player(String n, Castle myCastle, Castle oponCastle, Map map, int team) {
	this.oponCastle = oponCastle;
	this.myCastle = myCastle;
	this.frontWar = new Front(this.frontSize, myCastle.getPositionX(), 80);
	this.name = n;
	this.rank = 1;
	this.team = team;
	this.map = map;
	direction = this.myCastle.getPositionX() > this.oponCastle.getPositionX() ? -1 : 1;
	color = this.myCastle.getPositionX() > this.oponCastle.getPositionX() ? Color.RED : Color.BLUE;
	this.money = 500;
    }

    public boolean update(GraphicsContext gc) {

	for (int i = 0; i < listOfWarriors.size(); i++) {
	    Warrior w = listOfWarriors.get(i);
	    w.setCollisionNone();

	    int newPosition = w.getPositionX() + (w.getWalkingSpeed() * w.getDirection());
	    if (w.getHealth() <= 0) {
		listOfWarriors.remove(i);
	    }

	    for (int j = 0; j < i; j++) {
		Warrior wCol = listOfWarriors.get(j);
		if ((newPosition - w.getRadius() * this.direction * -1 / 2) * this.direction >= (wCol.getPositionX() + wCol.getRadius() * this.direction * -1 / 2) * this.direction) {
		    w.setCollisionUnit();
		    break;
		}
	    }

	    if (this.direction < 0) {
		if (this.oponCastle.getPositionX() + this.oponCastle.getRadius() / 2 >= newPosition) {
		    w.setCollisionCastle();
		}
	    } else if (this.oponCastle.getPositionX() - this.oponCastle.getRadius() / 2 <= newPosition) {
		w.setCollisionCastle();
	    }

	    // pokud je bez kolize
	    if (w.getCollisionStatus() == 0) {
		w.setPositionX(newPosition);
		w.setPositionY(map.getTerrainY(w.getPositionX()));
		w.doStep();
	    }
	    w.draw(gc);
	}
	
	if(this.frontWar.update(listOfWarriors) == true) {
	    return true;
	}else{
	return false;
	}

    }

    public Front getFrontWar() {
	return frontWar;
    }

    private void addWarrior(Warrior w) {
	this.listOfWarriors.add(w);
    }

    public int getMoney() {
	return money;
    }

    public void changeMoney(int value) {
	this.money += value;
    }

    public void createWarrior(constants.Category c, int trainingDelay) {
	// má na to peníze?
	// this.addWarrior(new Warrior(this.rank, c, this));
	Warrior w = new Warrior(this.rank, c, this);
	w.deductTrainignTime(trainingDelay);
	this.frontWar.addWarrior(w);
    }

    public Castle getMyCastle() {
	return this.myCastle;
    }

    public int getTeam() {
	return team;
    }

    public int getWarriorsCount() {
	return this.listOfWarriors.size();
    }

    public int getDirection() {
	return direction;
    }

    public Map getMap() {
	return map;
    }

    public void setFrontWar(Front frontWar) {
	this.frontWar = frontWar;
    }

    public Warrior getFirstWarrior() {
	int coord = this.myCastle.getPositionX() > this.oponCastle.getPositionX() ? 2000 : 0;
	int index = 0;
	if (coord == 0) {
	    for (int i = 0; i < this.listOfWarriors.size(); i++) {
		Warrior w = this.listOfWarriors.get(i);
		if (w.getPositionX() < coord) {
		    index = i;
		}
	    }
	    listOfWarriors.get(index).setFirst(true);
	    return listOfWarriors.get(index);

	} else {
	    for (int i = 0; i < this.listOfWarriors.size(); i++) {
		Warrior w = this.listOfWarriors.get(i);
		if (w.getPositionX() > coord) {
		    index = i;
		}
	    }
	    listOfWarriors.get(index).setFirst(true);
	    return listOfWarriors.get(index);
	}
    }

    public Warrior getFirstLeftShooter() {
   
	boolean founded = false;
	if (this.listOfWarriors.size() >= 1) {
	    int start = this.getMyCastle().getPositionX();
	    int candidateIndex = 0;
	    for (int i = 0; i < this.listOfWarriors.size(); i++) {
		Warrior w = this.listOfWarriors.get(i);
	
		if (w.getPositionX() > start && w.getCategory() == constants.Category.B){
		    candidateIndex = i;
		    start = this.listOfWarriors.get(candidateIndex).getPositionX();
		    founded = true;
		}
	    }
	    if(founded){
		return this.listOfWarriors.get(candidateIndex);
	    }else{
		return null;
	    }
	} else {
	    return null;
	}

    }
    
    public Warrior getFirstRightShooter() {
   
	//System.err.println("get right shooter");
	boolean founded = false;
	if (this.listOfWarriors.size() >= 1) {
	  //  System.err.println("jsou tu jednotky");
	    int start = this.getMyCastle().getPositionX();
	    //System.err.println("hrad: "+start);
	    int candidateIndex = 0;
	    for (int i = 0; i < this.listOfWarriors.size(); i++) {
		Warrior w = this.listOfWarriors.get(i);
		//System.err.println("vojak x: "+w.getPositionX());
		if (w.getPositionX() < start && w.getCategory() == constants.Category.B){
		    candidateIndex = i;
		    start = this.listOfWarriors.get(candidateIndex).getPositionX();
		    founded = true;
		}
	    }
	    if(founded){
		return this.listOfWarriors.get(candidateIndex);
	    }else{
		return null;
	    }
	} else {
	    return null;
	}

    }

    public List<Warrior> getListOfWarriors() {
	return listOfWarriors;
    }

    public String getName() {
	return name;
    }

    public Color getColor() {
	return color;
    }
}
