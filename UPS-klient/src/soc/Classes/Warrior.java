/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package soc.Classes;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import soc.constants;

/**
 *
 * @author Eflyax
 */
public class Warrior {

    private int positionX, positionY;
    private int health;
    private int maxHealth;
    private int damage;
    private int defense;
    private int walkingSpeed;
    private final int walkingSpeedDefault;
    private int trainingTimeDefault;
    private int fightingSpeed;
    private int price;
    private int direction;
    private int rank;
    private int healthBarSize;
    private int trainingTime;
    private Image texture;
    private Image textureFight;
    int textureFightSize = 30;
    private Player owner;
    private int radius;
    private int footStep;
    private int height;
    private boolean fighting;
    CollisionStatus cs;
    constants.Category category;
    private int tick;
    private int team;
    private boolean first;
    private boolean firstShooter;

    public Warrior(int rank, constants.Category c, Player owner) {
	this.tick = 0;
	this.cs = CollisionStatus.None;
	this.healthBarSize = 50;
	this.maxHealth = 100;
	this.fighting = false;
	this.owner = owner;
	this.category = c;
	this.damage = 2;
	this.positionX = this.owner.getMyCastle().getPositionX();
	this.direction = owner.getDirection();
	this.radius = 48;
	this.height = 96;
	this.fightingSpeed = 10;
	this.footStep = 0;
	this.textureFight = new Image(getClass().getResource("/soc/images/"+this.category.toString()+".png").toExternalForm());
	
	switch (this.category) {
	    case A:
		this.category = constants.Category.A;
		this.walkingSpeed = 3;
		this.fightingSpeed = 7;
		this.damage = 3;
		this.maxHealth = 25;
		this.trainingTime = 30;
		this.texture = new Image(getClass().getResource("/soc/images/sprite/" + owner.getTeam() + "/A.png").toExternalForm());
		break;
	    case B:
		this.category = constants.Category.B;
		this.walkingSpeed = 5;
		this.fightingSpeed = 10;
		this.damage = 3;
		this.trainingTime = 50;
		this.texture = new Image(getClass().getResource("/soc/images/sprite/" + owner.getTeam() + "/B.png").toExternalForm());
		break;
	    case C:
		this.category = constants.Category.C;
		this.walkingSpeed = 2;
		this.maxHealth = 50;
		this.damage = 5;
		this.fightingSpeed = 10;
		this.trainingTime = 100;
		this.texture = new Image(getClass().getResource("/soc/images/sprite/" + owner.getTeam() + "/C.png").toExternalForm());
		break;
	}
	this.trainingTimeDefault = this.trainingTime;
	this.walkingSpeedDefault = this.walkingSpeed;
	this.health = this.maxHealth;
    }

    public constants.Category getCategory() {
	return category;
    }

    public boolean isFirstShooter() {
	return firstShooter;
    }

    public void setFirstShooter(boolean firstShooter) {
	this.firstShooter = firstShooter;
    }

    public boolean isFirst() {
	return first;
    }

    public void setFirst(boolean first) {
	this.first = first;
    }

    public void setHealth(int health) {
	this.health = health;
    }

    public void setCollisionNone() {
	this.cs = CollisionStatus.None;
    }

    public void setCollisionUnit() {
	this.cs = CollisionStatus.Unit;
    }

    public void setCollisionCastle() {
	this.cs = CollisionStatus.Castle;
    }

    public void setCollisionFight() {
	this.cs = CollisionStatus.Fight;
    }

    public boolean isFighting() {
	return fighting;
    }

    public void setFighting(boolean fighting) {
	this.fighting = fighting;
    }

    
    public int getTrainingTimeDefault() {
	return trainingTimeDefault;
    }

    public void deductTrainignTime(int value) {
	this.trainingTimeDefault -= value;
    }

    public void setTrainingTime(int trainingTime) {
	this.trainingTime = trainingTime;
    }

    public void draw(GraphicsContext gc) {
	double onePr = (double) ((double) this.healthBarSize / (double) this.maxHealth);

	double x = (double) (this.positionX - radius / 2);
	double y = (double) (this.positionY - this.height);

	double spriteX = 96;
	double spriteY = 96;

	int row = 0;
	if (this.direction == 1) {
	    row = 1;
	}

	if(this.isFirst()){
	    gc.setFill(Color.CHOCOLATE);
	    gc.fillRect(this.positionX-4, this.positionY-this.height-this.healthBarSize-5, 8, this.healthBarSize);
	    gc.setFill(Color.GREEN);
	    gc.fillRect(this.positionX+2-4, this.positionY-this.height-this.healthBarSize-5, 4, (double)this.health*onePr);
	}
	
	gc.drawImage(this.texture, this.height * this.footStep, this.height + row * this.height, (0 + 96), (0 + 96), x, y + this.radius, this.radius, this.radius);
	//System.err.println(""+this.textureFight.getWidth());
	if(this.isFighting()){
	    if(this.direction == 1){
	       gc.drawImage(this.textureFight, 1 * this.tick%8, 0, (0 + textureFightSize), (0 + textureFightSize), x+10, y + textureFightSize, textureFightSize*2, textureFightSize*2);
	    }else{
	       gc.drawImage(this.textureFight, 1 * this.tick%8, 0, (0 + textureFightSize), (0 + textureFightSize), x+textureFightSize+10, y + textureFightSize, -textureFightSize*2, textureFightSize*2);
	    }
	}
	
    
    }

    private enum CollisionStatus {
	None, Unit, Castle, Fight
    };

    public int getCollisionStatus() {
	switch (this.cs) {
	    case Unit:
		return 1;
	    case Castle:
		return 2;
	    case Fight:
		return 3;
	    default:
		return 0;
	}
    }

    public int getWalkingSpeedDefault() {
	return walkingSpeedDefault;
    }

    public void setWalkingSpeedDeafault() {
	this.walkingSpeed = this.walkingSpeedDefault;
    }

    public void setWalkingSpeed(int value) {
	this.walkingSpeed = value;
    }

    public int getWalkingSpeed() {
	return walkingSpeed;
    }

    public void update() {

    }

    public int getPositionX() {
	return positionX;
    }

    public int getPositionY() {
	return positionY;
    }

    public void setDamage(int damage) {
	this.damage = damage;
    }
    
    

    public Player getOwner() {
	return owner;
    }

    public void setOwner(Player owner) {
	this.owner = owner;
    }

    public int getTrainingTime() {
	return trainingTime;
    }

    public void lowerTrainingTime() {
	this.trainingTime--;
    }

    public void setPositionY(int positionY) {
	this.positionY = positionY;
    }

    public int getDirection() {
	return direction;
    }

    public int getRadius() {
	return radius;
    }

    public void setPositionX(int positionX) {
	this.positionX = positionX;
    }

    public Rectangle getRectangle() {
	return new Rectangle(this.positionX, this.positionY, this.radius, this.height);
    }

    public int getLeftSide() {
	return this.positionX + this.radius / 2;
    }

    public int getRightSide() {
	return this.positionX - this.radius / 2;
    }

    public void setDirection(int direction) {
	this.direction = direction;
    }

    public int getDamage() {
	return damage;
    }

    public int getFightingSpeed() {
	return fightingSpeed;
    }

    public void giveDamage(int damage) {
	this.health -= damage;
    }

    public int getHealth() {
	return health;
    }

    public void doStep() {
	tick++;
	if (this.walkingSpeed != 0) {
	    if (tick > 5) {
		this.footStep++;
		tick = 0;
	    }
	    if (this.footStep >= 3) {
		this.footStep = 0;
	    }
	}

    }
}
