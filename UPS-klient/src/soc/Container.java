/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package soc;

import java.util.ArrayList;
import java.util.List;
import soc.Classes.Front;
import soc.Classes.Player;
import soc.Classes.Warrior;

/**
 *
 * @author eflyax
 */
public class Container {

    String name;
    String leftName;
    String rightName;
    int leftTeam;
    int rightTeam;
    final int MAX_TEAM = 3;
    String gameSeed;
    boolean leader;
    boolean init;
    String rejoinString;
    int leftHP;
    int rightHP;
    int leftMoney;
    int rightMoney;
    boolean switchToGame;
    boolean leftReady;
    boolean rightReady;
    int room = 0;
   
    String leftListFront;
    String leftListWar; 
    String rightListFront;
    String rightListWar;
    
    int rejoinRoom;

    public Container() {
	name = "Player";
	leftName = "";
	rightName = "";
	this.cleanUp();
    }
    
    public void cleanUp(){
    	
	leftTeam = 1;
	rightTeam = 1;
	gameSeed = "SoulOfCastles";
	leader = false;
	init = false;
	switchToGame = false;
	leftHP = 500;
	rightHP = 500;
	leftReady = false;
	rightReady = false;
    }

   public String getLeftRejoinString(Player playerLeft) {
	// data hráč: tým,jméno,ready_state,zdraví_pevnosti,peníze,jednotky_fronta,jednotky_boj
	StringBuilder rsb = new StringBuilder();
	//rsb.append("rejoin_string;left;");
	rsb.append(this.getLeftTeam()).append(":");
	rsb.append(this.getLeftName()).append(":");
	if(this.leftReady){// ready stav
	    rsb.append("1:");
	}else{
	    rsb.append("0:");
	}
	if (playerLeft == null) {//hráč ještě neexistuje
	    rsb.append("x:");// castleHP
	    rsb.append("x:");// peníze
	    rsb.append("x:");// vojáci ve frontě
	    rsb.append("x");// vojáci v boji
	  
	} else {
	    rsb.append(playerLeft.getMyCastle().getHealth()).append(":");
	    rsb.append(playerLeft.getMoney()).append(":");

	    // fronta
	    rsb.append(playerLeft.getFrontWar().getFrontToString());
	    
	    // vojáci v boji
	    if(playerLeft.getListOfWarriors().size() >= 1){
		for (int i = 0; i < playerLeft.getListOfWarriors().size(); i++) {
		    rsb.append(playerLeft.getListOfWarriors().get(i).getCategory().toString()).append("#");//kategorie#
		    rsb.append(playerLeft.getListOfWarriors().get(i).getPositionX()).append("*");//poziceX-
		}
		rsb.append(playerLeft.getFirstWarrior().getHealth());
	    }else{
		rsb.append("x");
	    }

	}
	return rsb.toString();
    }
    
    public String getRightRejoinString(Player playerRight) {
	// data hráč: tým,jméno,ready_state,zdraví_pevnosti,peníze,jednotky_fronta,jednotky_boj
	StringBuilder rsb = new StringBuilder();
	rsb.append(this.getRightTeam()).append(":");
	rsb.append(this.getRightName()).append(":");
	if(this.rightReady){// ready stav
	    rsb.append("1:");
	}else{
	    rsb.append("0:");
	}
	if (playerRight == null) {//hráč ještě neexistuje
	    rsb.append("x:");// castleHP
	    rsb.append("x:");// peníze
	    rsb.append("x:");// vojáci ve frontě
	    rsb.append("x");// vojáci v boji
	} else {
	    rsb.append(playerRight.getMyCastle().getHealth()).append(":");
	    rsb.append(playerRight.getMoney()).append(":");

	    // fronta
	    rsb.append(playerRight.getFrontWar().getFrontToString());

	     // vojáci v boji
	    if(playerRight.getListOfWarriors().size() >= 1){
		for (int i = 0; i < playerRight.getListOfWarriors().size(); i++) {
		rsb.append(playerRight.getListOfWarriors().get(i).getCategory().toString()).append("#");//kategorie#
		rsb.append(playerRight.getListOfWarriors().get(i).getPositionX()).append("*");//poziceX-
		}
		rsb.append(playerRight.getFirstWarrior().getHealth());
	    }else{
		rsb.append("x");
	    }
	    
	}
	return rsb.toString();
    }
    
    public String getMainRejoinString(Player playerLeft, Player playerRight){
	
	StringBuilder sb = new StringBuilder();
	sb.append("rejoin_string;");
	sb.append(this.getLeftRejoinString(playerLeft));
	sb.append(";");
	sb.append(this.getRightRejoinString(playerRight));
	sb.append(";");
	sb.append(this.getGameSeed());
	sb.append(";");
	sb.append(this.getRoom());
	return sb.toString();
    }

    public int getLeftHP() {
	return leftHP;
    }

    public void setLeftHP(int leftHP) {
	this.leftHP = leftHP;
    }

    public int getRightHP() {
	return rightHP;
    }

    public void setRightHP(int rightHP) {
	this.rightHP = rightHP;
    }

    
    public boolean isLeader() {
	return leader;
    }

    public String getRejoinString() {
	return rejoinString;
    }

    public void setRejoinString(String rejoinString) {
	this.rejoinString = rejoinString;
    }

    public String getName() {
	return name;
    }

    public boolean isSwitchToGame() {
	return switchToGame;
    } 
    
    public void setName(String name) {
	this.name = name;
    }
    
    
    
    public void applyRejoinString() {

//rejoin_string;1:Eflyax:1:500:500:B#A#&17:C#223*C#174*C#125*B#100*100;1:koko:1:500:500:B#A#A#A#&31:C#941-C#990-B#1011-100;ctnuvwvecw;1
	String[] data = this.rejoinString.split(";");
	
	if(data.length == 5){ // data mají správný počet
	    
	    String flag = data[0];
	    String[] dlp = data[1].split(":");
	    String[] drp = data[2].split(":");
	    String GameSeed = data[3];

	    try{
	    leftTeam = Integer.parseInt(dlp[0]);
	    }catch(Exception e){
		System.err.println("chyb při zpracování levého týmu");
	    }
	    leftName = dlp[1];
	    
	    try{
	    if(Integer.parseInt(dlp[2]) == 1){//hráč byl ve hře, nastavím herní data
		try{
		    leftReady = true;
		    leftHP = Integer.parseInt(dlp[3]);
		    leftMoney = Integer.parseInt(dlp[4]);
		    
		    if(!dlp[5].equals("x")){// má vojáky ve frontě
			this.setLeftListFront(dlp[5]);
		    }else{
		    	this.setLeftListFront("");
		    }
		    // data fronty = drp[5]
		    if(!dlp[6].equals("x")){// má vojáky ve frontě
			this.setLeftListWar(dlp[6]);
		    }else{
		    	this.setLeftListWar("");
		    }
		    
		}catch(Exception e){
		
		}
	    }
	    }catch(Exception e){
	    
	    }
	   
	    
	    rightTeam = Integer.parseInt(drp[0]);
	    rightName = drp[1];
	    
	    if(Integer.parseInt(drp[2]) == 1){//hráč byl ve hře, nastavím herní data
		try{
		    rightReady = true;
		    rightHP = Integer.parseInt(drp[3]);
		    rightMoney = Integer.parseInt(drp[4]);
		    
		    // data fronty = drp[5]
		    if(!drp[5].equals("x")){// má vojáky ve frontě
			this.setRightListFront(drp[5]);
		    }else{
		    	this.setRightListFront("");
		    }
		    // data fronty = drp[5]
		    if(!drp[6].equals("x")){// má vojáky ve frontě
			this.setRightListWar(drp[6]);
		    }else{
		    	this.setRightListWar("");
		    }
		}catch(Exception e){
		
		}
	    }
	    
	    
	    
	    if(this.getName().equals(leftName)){
		this.setLeader(true);
		if(Integer.parseInt(dlp[2]) == 1){
		    switchToGame = true;
		}
	    }else{
		this.setLeader(false);
		if(Integer.parseInt(drp[2]) == 1){
		    switchToGame = true;
		}
	    }
	    
	    this.gameSeed = GameSeed;
	    this.room = Integer.parseInt(data[4]);
	    
	}else{
	    System.err.println("Přijatá data ze serveru jsou poškozená: "+this.rejoinString);
	}
    }

    

    public boolean isInit() {
	return init;
    }

    public void setInit(boolean init) {
	this.init = init;
    }

    public void setLeader(boolean leader) {
	this.leader = leader;
    }

    public String getLeftName() {
	return leftName;
    }

    public void setLeftName(String yourName) {
	this.leftName = yourName;
    }

    public String getRightName() {
	return rightName;
    }

    public void setRightName(String opponentName) {
	this.rightName = opponentName;
    }

    public int getLeftTeam() {
	return leftTeam;
    }
    
    public void setSwitchLeftTeam(int yourTeam) {
	this.leftTeam += yourTeam;
	if (this.leftTeam >= MAX_TEAM) {
	    this.leftTeam = 1;
	}
	if (this.leftTeam <= 0) {
	    this.leftTeam = MAX_TEAM-1;
	}
    }

    public void setLeftListFront(String leftListFront) {
	this.leftListFront = leftListFront;
    }

    public void setLeftListWar(String leftListWar) {
	this.leftListWar = leftListWar;
    }

    public void setRightListFront(String rightListFront) {
	this.rightListFront = rightListFront;
    }

    public void setRightListWar(String rightListWar) {
	this.rightListWar = rightListWar;
    }

    public String getLeftListFront() {
	return leftListFront;
    }

    public String getLeftListWar() {
	return leftListWar;
    }

    public String getRightListFront() {
	return rightListFront;
    }

    public String getRightListWar() {
	return rightListWar;
    }

    public int getRightTeam() {
	return rightTeam;
    }

    public void setSwitchRightTeam(int opponentTeam) {
	this.rightTeam += opponentTeam;
	if (this.rightTeam >= MAX_TEAM) {
	    this.rightTeam = 1;
	}
	if (this.rightTeam <= 0) {
	    this.rightTeam = MAX_TEAM-1;
	}
    }

    public void setLeftTeam(int value) {
	this.leftTeam = value;
    }

    public void setRightTeam(int value) {
	this.rightTeam = value;
    }

    public String getGameSeed() {
	return gameSeed;
    }

    public void setGameSeed(String gameSeed) {
	this.gameSeed = gameSeed;
    }
    
        public int getRejoinRoom() {
	return rejoinRoom;
    }

    public void setRejoinRoom(int rejoinRoom) {
	this.rejoinRoom = rejoinRoom;
    }

    public boolean isLeftReady() {
	return leftReady;
    }

    public void setLeftReady(boolean leftReady) {
	this.leftReady = leftReady;
    }

    public boolean isRightReady() {
	return rightReady;
    }

    public void setRightReady(boolean rightReady) {
	this.rightReady = rightReady;
    }

    public int getRoom() {
	return room;
    }

    public void setRoom(int room) {
	this.room = room;
    }    
}
