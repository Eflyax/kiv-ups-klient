/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package soc.Controllers;

import javafx.fxml.Initializable;

/**
 *
 * @author eflyax
 */
public abstract class Controller extends Thread implements Initializable  {
	
	
	public abstract void processMessage(String data);	
}
