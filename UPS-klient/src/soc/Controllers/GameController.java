package soc.Controllers;

import javafx.scene.input.KeyEvent;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Rectangle2D;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import soc.Classes.Castle;
import soc.Classes.Map;
import soc.Classes.Player;
import soc.Classes.Warrior;
import soc.Main;
import soc.constants;

/**
 *
 * @author Eflyax
 */
public class GameController extends Controller {

    @FXML
    private AnchorPane AnchorPane;
    @FXML
    private Canvas terrainCanvas;
    @FXML
    private Canvas warriorCanvas;
    @FXML
    private GraphicsContext layerTerrain;
    @FXML
    private GraphicsContext layerWarrior;
    @FXML
    private Button categoryA;
    @FXML
    private Button categoryB;
    @FXML
    private Button categoryC;
    @FXML
    private Button give_up;
    @FXML
    private Button notWait;
    @FXML
    private Pane paneWaiting;
    @FXML
    private Label leaderName;
    @FXML
    private Label opponentName;
    @FXML
    private Label labelWaiting;
    @FXML
    private HBox hbox;

    private Main application;
    private Map map;
    private Castle castleL;
    private Castle castleR;
    private Player leftPlayer;
    private Player rightPlayer;

    boolean running = false;
    boolean win = false;
    //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
    boolean debugMode = false;
    //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
    volatile boolean shouldIRun = false;
    boolean ready_message = false;

    Warrior leftW;
    Warrior rightW;
    Warrior leftWshooter;
    Warrior rightWshooter;

    public void setApp(Main application) {

	this.win = false;
	this.application = application;
	layerTerrain = terrainCanvas.getGraphicsContext2D();
	layerWarrior = warriorCanvas.getGraphicsContext2D();
	map = new Map();

	paneWaiting.toFront();
	paneWaiting.setVisible(true);
	labelWaiting.setText("   Hra automaticky začne, jakmile bude protihráč připraven");
	notWait.setVisible(false);

	if (application.getContainer().isLeftReady() && application.getContainer().isRightReady()) {
	    processReady();
	}

	if (application.getContainer().isLeader()) {
	    ImageView imgview_A = new ImageView(new Image(getClass().getResource("/soc/images/sprite/" + Integer.toString(application.getContainer().getLeftTeam()) + "/A.png").toExternalForm()));
	    Rectangle2D a_viewPort = new Rectangle2D(96, 0, 96, 96);
	    imgview_A.setViewport(a_viewPort);
	    imgview_A.setFitWidth(40);
	    imgview_A.setFitHeight(40);
	    categoryA.setGraphic(imgview_A);

	    ImageView imgview_B = new ImageView(new Image(getClass().getResource("/soc/images/sprite/" + Integer.toString(application.getContainer().getLeftTeam()) + "/B.png").toExternalForm()));
	    Rectangle2D b_viewPort = new Rectangle2D(96, 0, 96, 96);
	    imgview_B.setViewport(b_viewPort);
	    imgview_B.setFitWidth(40);
	    imgview_B.setFitHeight(40);
	    categoryB.setGraphic(imgview_B);

	    ImageView imgview_C = new ImageView(new Image(getClass().getResource("/soc/images/sprite/" + Integer.toString(application.getContainer().getLeftTeam()) + "/C.png").toExternalForm()));
	    Rectangle2D c_viewPort = new Rectangle2D(96, 0, 96, 96);
	    imgview_C.setViewport(c_viewPort);
	    imgview_C.setFitWidth(40);
	    imgview_C.setFitHeight(40);
	    categoryC.setGraphic(imgview_C);

	} else {
	    ImageView imgview_A = new ImageView(new Image(getClass().getResource("/soc/images/sprite/" + Integer.toString(application.getContainer().getRightTeam()) + "/A.png").toExternalForm()));
	    Rectangle2D a_viewPort = new Rectangle2D(96, 0, 96, 96);
	    imgview_A.setViewport(a_viewPort);
	    imgview_A.setFitWidth(40);
	    imgview_A.setFitHeight(40);
	    categoryA.setGraphic(imgview_A);

	    ImageView imgview_B = new ImageView(new Image(getClass().getResource("/soc/images/sprite/" + Integer.toString(application.getContainer().getRightTeam()) + "/B.png").toExternalForm()));
	    Rectangle2D b_viewPort = new Rectangle2D(96, 0, 96, 96);
	    imgview_B.setViewport(b_viewPort);
	    imgview_B.setFitWidth(40);
	    imgview_B.setFitHeight(40);
	    categoryB.setGraphic(imgview_B);

	    ImageView imgview_C = new ImageView(new Image(getClass().getResource("/soc/images/sprite/" + Integer.toString(application.getContainer().getRightTeam()) + "/C.png").toExternalForm()));
	    Rectangle2D c_viewPort = new Rectangle2D(96, 0, 96, 96);
	    imgview_C.setViewport(c_viewPort);
	    imgview_C.setFitWidth(40);
	    imgview_C.setFitHeight(40);
	    categoryC.setGraphic(imgview_C);
	}
	categoryA.setDisable(true);
	categoryB.setDisable(true);
	categoryC.setDisable(true);

	ImageView giveup_view = new ImageView(new Image(getClass().getResource("/soc/images/giveup.png").toExternalForm()));
	give_up.setGraphic(giveup_view);

	if (debugMode) {
	    debugProcess();
	}

    }// konec setapp

    public void debugProcess() {

	application.getContainer().setLeader(true);
	//zpráva od serveru, že hra může začít
	notWait.setVisible(true);
	paneWaiting.setVisible(false);

	seedToParams();
	castleL = new Castle(Castle.Side.LEFT, application.getContainer().getLeftTeam());
	castleR = new Castle(Castle.Side.RIGHT, application.getContainer().getRightTeam());
	map.setCastles(castleL, castleR, layerTerrain);
	initListeners();
	initPlayers();
	getActive();
	start();
	ready_message = true;

	// inicializovat herní data z kontejneru
	this.parseFront(leftPlayer, application.getContainer().getLeftListFront());
	this.parseBattleGround(leftPlayer, application.getContainer().getLeftListWar());
	this.parseFront(rightPlayer, application.getContainer().getRightListFront());
	this.parseBattleGround(rightPlayer, application.getContainer().getRightListWar());
	this.leftPlayer.getMyCastle().setHealth(application.getContainer().getLeftHP());
	this.rightPlayer.getMyCastle().setHealth(application.getContainer().getRightHP());

	categoryA.setDisable(false);
	categoryB.setDisable(false);
	categoryC.setDisable(false);
	shouldIRun = true;

	Button switchPlayer = new Button("-> <-");
	hbox.getChildren().add(switchPlayer);
	switchPlayer.setOnAction((event) -> {
	    if (application.getContainer().isLeader()) {
		application.getContainer().setLeader(false);
	    } else {
		application.getContainer().setLeader(true);
	    }
	});

	Warrior wl = new Warrior(0, constants.Category.A, leftPlayer);
	wl.setPositionX(340);
//wl.setDamage(0);
	leftPlayer.getListOfWarriors().add(wl);

	Warrior wr = new Warrior(0, constants.Category.C, rightPlayer);
	wr.setPositionX(380);
//wr.setDamage(0);
	rightPlayer.getListOfWarriors().add(wr);

    }// konec debug modu

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    private Player getActive() {
	return application.getContainer().isLeader() ? leftPlayer : rightPlayer;
    }

    private void initPlayers() {

	if (application.getContainer().isLeader()) {

	    this.leftPlayer = new Player(application.getContainer().getLeftName(), castleL, castleR, map, application.getContainer().getLeftTeam()); // jsem vedouci (vlevo)
	    this.rightPlayer = new Player(application.getContainer().getRightName(), castleR, castleL, map, application.getContainer().getRightTeam());  // oponent (vpravo)
	    Platform.runLater(() -> {
		leaderName.setText(leftPlayer.getName());
		opponentName.setText(rightPlayer.getName());
	    });
	} else {
	    this.leftPlayer = new Player(application.getContainer().getRightName(), castleL, castleR, map, application.getContainer().getLeftTeam()); // jsem ooponent (vpravo)
	    this.rightPlayer = new Player(application.getContainer().getLeftName(), castleR, castleL, map, application.getContainer().getRightTeam());  // je leader (vlevo)
	    Platform.runLater(() -> {
		leaderName.setText(rightPlayer.getName());
		opponentName.setText(leftPlayer.getName());
	    });
	}
    }

    private void processReady() {
	if (!ready_message) {

	    //zpráva od serveru, že hra může začít
	    notWait.setVisible(true);
	    paneWaiting.setVisible(false);

	    seedToParams();
	    castleL = new Castle(Castle.Side.LEFT, application.getContainer().getLeftTeam());
	    castleR = new Castle(Castle.Side.RIGHT, application.getContainer().getRightTeam());
	    map.setCastles(castleL, castleR, layerTerrain);
	    initListeners();
	    initPlayers();
	    getActive();
	    start();
	    ready_message = true;

	    // inicializovat herní data z kontejneru
	    this.parseFront(leftPlayer, application.getContainer().getLeftListFront());
	    this.parseBattleGround(leftPlayer, application.getContainer().getLeftListWar());
	    this.parseFront(rightPlayer, application.getContainer().getRightListFront());
	    this.parseBattleGround(rightPlayer, application.getContainer().getRightListWar());
	    this.leftPlayer.getMyCastle().setHealth(application.getContainer().getLeftHP());
	    this.rightPlayer.getMyCastle().setHealth(application.getContainer().getRightHP());
	}

	shouldIRun = true;
	paneWaiting.setVisible(false);
	Platform.runLater(() -> {
	    categoryA.setDisable(false);
	    categoryB.setDisable(false);
	    categoryC.setDisable(false);
	});
	initWarriorStatus();
    }

    private void parseFront(Player player, String frontString) {

	if (frontString != null && !frontString.isEmpty()) {//mam data -> parsuji je	    

	    String[] data = frontString.split("&");
	    String[] units = data[0].split("#");

	    for (int i = 0; i < units.length; i++) {
		Warrior w = new Warrior(0, constants.chatToCategory(units[i]), player);
		if (i == 0) {
		    w.setTrainingTime(Integer.parseInt(data[1]));
		}
		player.getFrontWar().addWarrior(w);
	    }
	}
    }

    private void parseBattleGround(Player player, String WarString) {

	if (WarString != null && !WarString.isEmpty()) {//mam data -> parsuji je
	    String[] units = WarString.split("\\*");
	    for (int i = 0; i < units.length - 1; i++) {

		String[] WarriorData = units[i].split("#");
		Warrior w = new Warrior(0, constants.chatToCategory(WarriorData[0]), player);
		w.setPositionX(Integer.parseInt(WarriorData[1]));
		player.getListOfWarriors().add(w);
	    }
	    player.getFirstWarrior().setHealth(Integer.parseInt(units[units.length - 1]));
	}
    }

    private void initListeners() {
	give_up.setOnAction((event) -> {
	    try {
		Platform.runLater(() -> {
		    Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
		    alert.setTitle("Vzdát se z boje");
		    alert.setHeaderText("Opravdu se chcete zvdát a nechát vyhrát vašeho soupeře?");
		    alert.setContentText("");

		    Optional<ButtonType> result;
		    result = alert.showAndWait();
		    if (result.get() == ButtonType.OK) {
			try {
			    application.getTCPclient().sendMessage("leave;");
			} catch (Exception e) {

			}
			shouldIRun = false;
			application.goToLobby();

		    } else {
			// ... user chose CANCEL or closed the dialog
		    }
		});
	    } catch (Exception e) {

	    }
	});

	categoryA.setOnAction((event) -> {
	    this.getActive().createWarrior(constants.Category.A, 0);
	    sendWarrior("attack;a");
	});
	categoryB.setOnAction((event) -> {
	    this.getActive().createWarrior(constants.Category.B, 0);
	    sendWarrior("attack;b");
	});
	categoryC.setOnAction((event) -> {
	    this.getActive().createWarrior(constants.Category.C, 0);
	    sendWarrior("attack;c");
	});

	notWait.setOnAction((event) -> {
	    Platform.runLater(() -> {
		Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
		alert.setTitle("Zrušit rozehranou hru");
		alert.setHeaderText("Chystáte se opustit rozehranou hru, do které se za chvíli může připojit váš protihráč.\nPokud hru opustíte, nemůžete se do ní vrátit.");
		alert.setContentText("Opravdu chcete místnost opustit?");

		Optional<ButtonType> result;
		result = alert.showAndWait();
		if (result.get() == ButtonType.OK) {
		    try {
			application.getTCPclient().sendMessage("leave;");
		    } catch (Exception e) {
		    }
		    shouldIRun = false;
		    application.goToLobby();
		} else {
		    // ... user chose CANCEL or closed the dialog
		}
	    });
	});

    }

    @FXML
    private void seedToParams() {
	map.modelTerrain(constants.seedToParametres(application.getContainer().getGameSeed()));
    }

    private void sendWarrior(String message) {
	try {
	    this.application.getTCPclient().sendMessage(message);
	} catch (Exception e) {
	    System.out.println("Chyba při odesílání vytvoření postavy na server");
	}
    }

    public void initWarriorStatus() {
	if (leftPlayer.getListOfWarriors().size() >= 1) {
	    leftW = leftPlayer.getFirstWarrior();
	    leftWshooter = leftPlayer.getFirstLeftShooter();
	}
	if (rightPlayer.getListOfWarriors().size() >= 1) {
	    rightW = rightPlayer.getFirstWarrior();
	    rightWshooter = rightPlayer.getFirstRightShooter();
	}
    }

    @Override
    @SuppressWarnings("empty-statement")
    public synchronized void run() {
	running = true;

	long lastLoopTime = System.nanoTime();
	final int TARGET_FPS = 30;
	final long OPTIMAL_TIME = 1000000000 / TARGET_FPS;

	int tick = 0;

	Platform.runLater(() -> {
	    try {
		warriorCanvas.toFront();
	    } catch (Exception e) {
	    }
	});

	map.clearCanvas(layerTerrain);
	map.draw(layerTerrain);
	castleL.draw(layerTerrain);
	castleR.draw(layerTerrain);

	while (running) {
	    while (shouldIRun == false) {
		try {
		    Thread.sleep(200);
		} catch (InterruptedException ex) {
		    Logger.getLogger(GameController.class.getName()).log(Level.SEVERE, null, ex);
		}
	    }

	    long now = System.nanoTime();
	    long updateLength = now - lastLoopTime;
	    lastLoopTime = now;
	    double delta = updateLength / ((double) OPTIMAL_TIME);
	    tick++;

	    initWarriorStatus();
	    try {

		if (leftPlayer.getWarriorsCount() > 0 && rightPlayer.getWarriorsCount() > 0) {

		    //Warrior leftW = leftPlayer.getFirstWarrior();
		    // Warrior rightW = rightPlayer.getFirstWarrior();
		    // 1. hráci se srazili
		    if (leftW.getPositionX() + leftW.getRadius() / 2 + 5 >= rightW.getPositionX() - rightW.getRadius() / 2) {
			leftW.setWalkingSpeed(0); //
			rightW.setWalkingSpeed(0);// vynulování rychlosti
			leftW.setCollisionFight();
			rightW.setCollisionFight();
			leftW.setFighting(true);
			rightW.setFighting(true);
			if (tick % leftW.getFightingSpeed() == 0) {
			    rightW.giveDamage(leftW.getDamage());
			}
			if (tick % rightW.getFightingSpeed() == 0) {
			    leftW.giveDamage(rightW.getDamage());
			}
			if (leftW.getHealth() <= 0) {
			    rightW.setWalkingSpeedDeafault();
			    rightW.setFighting(false);
			}
			if (rightW.getHealth() <= 0) {
			    leftW.setWalkingSpeedDeafault();
			    leftW.setFighting(false);
			}
		    }

		    // Warrior leftWshooter = leftPlayer.getFirstLeftShooter();
		    if (leftWshooter != null) {
			leftWshooter.setFirstShooter(true);

			if (leftWshooter.getPositionX() + leftWshooter.getRadius() / 2 + 5 >= rightW.getPositionX() - rightW.getRadius() * 3) {
			    leftWshooter.setFighting(true);
			    if (tick % leftWshooter.getFightingSpeed() == 0) {

				rightW.giveDamage(leftWshooter.getDamage());
			    }
			    if (rightW.getHealth() <= 0) {
				leftWshooter.setWalkingSpeedDeafault();
				leftWshooter.setFighting(false);
				leftW.setWalkingSpeedDeafault();
			    }
			}
		    }

		    //  Warrior rightWshooter = rightPlayer.getFirstRightShooter();
		    if (rightWshooter != null) {
			rightWshooter.setFirstShooter(true);

			if (rightWshooter.getPositionX() + rightWshooter.getRadius() / 2 - 5 <= leftW.getPositionX() + leftW.getRadius() * 3) {
			    rightWshooter.setFighting(true);
			    if (tick % rightWshooter.getFightingSpeed() == 0) {

				leftW.giveDamage(rightWshooter.getDamage());
			    }
			    if (leftW.getHealth() <= 0) {
				rightWshooter.setWalkingSpeedDeafault();
				rightWshooter.setFighting(false);
				rightW.setWalkingSpeedDeafault();
			    }
			}
		    }
		}

	    } catch (Exception e) {
	    }

	    try {
		if (leftPlayer.getWarriorsCount() > 0) {
		    // Warrior leftW = leftPlayer.getFirstWarrior();
		    if (leftW.getCollisionStatus() == 2) {
			castleR.setDamage(leftW.getDamage());
			try {
			    if (application.getContainer().isLeader()) {
				application.getTCPclient().sendMessage("setdmg;" + leftW.getDamage());
			    }
			} catch (Exception e) {
			}

			if (castleR.getHealth() <= 0) {
			    //levý hráč vyhrál
			    shouldIRun = false;
			}
		    }
		}

	    } catch (Exception e) {
	    }

	    try {
		if (rightPlayer.getWarriorsCount() > 0) {
		    // Warrior rightW = rightPlayer.getFirstWarrior();
		    if (rightW.getCollisionStatus() == 2) {
			castleL.setDamage(rightW.getDamage());
			try {
			    if (!application.getContainer().isLeader()) {

				application.getTCPclient().sendMessage("setdmg;" + rightW.getDamage());
			    }
			} catch (Exception e) {
			}
			if (castleL.getHealth() <= 0) {
			    //pravý hráč vyhrál
			    shouldIRun = false;
			}
		    }
		}

	    } catch (Exception e) {
	    }

	    try {
		layerWarrior.clearRect(0, 0, layerWarrior.getCanvas().getWidth(), layerWarrior.getCanvas().getHeight());

		leftPlayer.update(layerWarrior);
		rightPlayer.update(layerWarrior);

		castleL.drawHealthBar(layerWarrior);
		castleR.drawHealthBar(layerWarrior);
		getActive().getFrontWar().draw(layerWarrior);
	    } catch (Exception e) {
	    }
	    try {
		Thread.sleep((lastLoopTime - System.nanoTime() + OPTIMAL_TIME) / 1000000);
	    } catch (Exception e) {
	    }

	    if (tick > 1000) {
		tick = 0;
	    }
	}
	//running = false;
    }

    @Override
    public void processMessage(String value) {

	String data[] = value.split(";");
	switch (data[0]) { // o jaky prikaz jde?
	    case "attack": // protihráč zautocil

		if (application.getContainer().isLeader()) {
		    rightPlayer.createWarrior(constants.chatToCategory(data[1]), Integer.parseInt(data[2]));
		} else {
		    leftPlayer.createWarrior(constants.chatToCategory(data[1]), Integer.parseInt(data[2]));
		}
		break;
	    case "norejoin":
		Platform.runLater(() -> {
		    Alert alert = new Alert(AlertType.CONFIRMATION);
		    alert.setTitle("Protihráč odmítl pokračovat");
		    alert.setHeaderText("Váš protihráč se znovu připojil na server, ale odmítl pokračovat va vaší rozehrané hře.\nTato rozehraná hra bude ukončena.");
		    // alert.setContentText("Choose your option.");
		    try {
			application.getTCPclient().sendMessage("leave;");

		    } catch (Exception e) {
		    }
		    ButtonType buttonTypeOne = new ButtonType("Rozumím");

		    alert.getButtonTypes().setAll(buttonTypeOne);

		    Optional<ButtonType> result = alert.showAndWait();
		    if (result.get() == buttonTypeOne) {
			//todo
			application.goToLobby();
		    } else {
			application.goToLobby();
		    }

		});
		break;
	    case "op_ready":
		if (application.getContainer().isLeader()) {
		    System.out.println("jsem levý, pravý je ready, nastavil jsem mu to");
		    application.getContainer().setRightReady(true);
		} else {
		    application.getContainer().setLeftReady(true);
		}
		break;
	    case "ready":
		
		processReady();

		break;
	    case "stop":
		shouldIRun = false;
		Platform.runLater(() -> {
		    paneWaiting.toFront();
		    Alert alert = new Alert(Alert.AlertType.WARNING);
		    alert.setTitle("Varování");
		    alert.setHeaderText("Protihráč má problémy s internetovým připojením a byl odpojen ze serveru.");
		    alert.setContentText("Můžete počkat na jeho návrat do hry");
		    alert.showAndWait();
		    paneWaiting.setVisible(true);
		    labelWaiting.setText("      Čekám na protihráče, až se znovu připojí do hry");
		});

		new Timer().schedule(
			new TimerTask() {
		    int i = 0;

		    @Override
		    public void run() {
			Platform.runLater(() -> {

			    if (i < 3) {
				i++;
				labelWaiting.setText(labelWaiting.getText() + ".");

			    } else {
				labelWaiting.setText("      Čekám na protihráče, až se znovu připojí do hry");
				i = 0;
			    }
			});
		    }
		}, 0, 1500);

		String rejoin = "";
		try {
		    if (leftPlayer == null && rightPlayer == null) {
			rejoin = application.getContainer().getMainRejoinString(null, null);
		    } else {
			rejoin = application.getContainer().getMainRejoinString(leftPlayer, rightPlayer);
		    }

		} catch (Exception e) {
		    System.err.println("chyba při vytvoření rejoin stringu " + e.getMessage() + ", " + rejoin.length());
		}
		try {
		    application.getTCPclient().sendMessage(rejoin);
		} catch (Exception e) {
		    System.err.println("chyba při odesílání rejoin stringu");
		}

		break;
	    case "change_team":
		if (application.getContainer().isLeader()) {
		    try {
			application.getContainer().setSwitchRightTeam(Integer.parseInt(data[1]));
		    } catch (Exception e) {
		    }
		} else {
		    try {
			application.getContainer().setSwitchLeftTeam(Integer.parseInt(data[1]));
		    } catch (Exception e) {
		    }
		}
		break;
	    case "disconnected":
		Platform.runLater(() -> {
		    if(data.length > 1){
			    Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Varování");
			alert.setHeaderText("Spojení se serverem bylo ztraceno. Klient je odopjen od serveru.");
			alert.setContentText("Zkuste se za chvíli připojit znovu");
			alert.showAndWait();
			    
		    }
		    application.goToLobby();
		});

		break;
	    case "win":
		if (!win) {
		    shouldIRun = false;
		    Platform.runLater(() -> {
			String showMessage = "";

			if (data.length > 1) {
			    // někdo vyhrál
			    if (application.getContainer().isLeader()) {
				if (leftPlayer.getMyCastle().getHealth() > rightPlayer.getMyCastle().getHealth()) {
				    showMessage = "Zvítězil jsi!";
				} else {
				    showMessage = "Byl jsi poražen";
				}
			    } else// je vpravo
			    {
				if (leftPlayer.getMyCastle().getHealth() < rightPlayer.getMyCastle().getHealth()) {
				    showMessage = "Zvítězil jsi!";
				} else {
				    showMessage = "Byl jsi poražen";
				}
			    }
			} else {
			    // protihráč se vzdal...
			    showMessage = "Protihráč se vzdal.";
			}
			Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
			alert.setTitle("Konec hry");
			alert.setHeaderText(showMessage);
			// alert.setContentText("Opravdu se chcete zvdát a nechát vyhrát vašeho soupeře?");

			Optional<ButtonType> result;
			result = alert.showAndWait();
			if (result.get() == ButtonType.OK) {
			    try {
				application.getTCPclient().sendMessage("leave;");

			    } catch (Exception e) {
			    }

			    application.getContainer().cleanUp();
			    shouldIRun = false;
			    application.goToLobby();

			} else {
			    try {
				application.getTCPclient().sendMessage("leave;");
			    } catch (Exception e) {

			    }
			    application.getContainer().cleanUp();
			    shouldIRun = false;
			    application.goToLobby();
			}
		    });
		}
		win = true;
		break;
	    default: // neznámý příkaz
		System.err.println("Unkown:" + data[0]);

		break;
	}

    }

}
