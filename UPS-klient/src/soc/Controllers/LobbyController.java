/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package soc.Controllers;

import java.io.BufferedWriter;
import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.geometry.Rectangle2D;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import javax.net.ssl.SSLEngineResult;
import soc.Classes.LobbyCell;

import soc.Main;

/**
 * FXML Controller class
 *
 * @author Eflyax
 */
public class LobbyController extends Controller {

    @FXML
    private Button pripojit;
    @FXML
    private Button odpojit;
    @FXML
    private Button send;
    @FXML
    private Button refresh_lobbyButton;
    @FXML
    private Button new_lobbyButton;
    @FXML
    private Button join_gameButton;
    @FXML
    private Button leave_lobbyButton;
    @FXML
    private Button start_game;
    @FXML
    private TextArea textArea;
    @FXML
    private TextField messageText;
    @FXML
    private TextField textBoxAdress;
    @FXML
    private TextField nickname_field;
    @FXML
    private TextField port_field;
    @FXML
    private ListView<LobbyCell> listLobby;
    @FXML
    private Label connectionStatus;
    @FXML
    private Label opponent_name;
    @FXML
    private Label leader_name;
    @FXML
    private Label seed_name;
    @FXML
    private ImageView left_team;
    @FXML
    private ImageView right_team;
    @FXML
    private Button change_team_left_down;
    @FXML
    private Button change_team_left_up;
    @FXML
    private Button change_team_right_down;
    @FXML
    private Button change_team_right_up;
    @FXML
    private VBox left_vbox;
    @FXML
    private VBox right_vbox;
    @FXML
    private Pane pane_rooms;
    @FXML
    private Pane pane_room;
    @FXML
    private Label label_numberRoom;

    private ObservableList<LobbyCell> items;
    BufferedWriter bw;
    private Main application;
    boolean handShake;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

	initListeners();
	items = FXCollections.observableArrayList();
	refresh_lobbyButton.getStyleClass().add("refresh");
	odpojit.setDisable(true);
	pane_rooms.setDisable(true);
	start_game.setDisable(true);
	send.setDisable(true);
	messageText.setDisable(true);
	pane_room.setVisible(false);
	pane_rooms.setVisible(true);
    }

    private void connectServer() {

	handShake = false;
	application.getContainer().setLeftName(nickname_field.getText());
	application.getContainer().setName(nickname_field.getText());
	application.setNewTCP();
	if (!application.getTCPclient().isAlive()) {
	    application.getTCPclient().initConnectionData(textBoxAdress.getText(), Integer.parseInt(port_field.getText()));
	    application.getTCPclient().start();

	    new java.util.Timer().schedule(
		    new java.util.TimerTask() {
		@Override
		public void run() {
		    Platform.runLater(() -> {
			if (application.getTCPclient().connected && !handShake) {
			    Alert alert = new Alert(AlertType.ERROR);
			    alert.setTitle("Chyba");
			    alert.setHeaderText("Server nedodržuje konvenci komunikačního protokolu. Zřejmě se nejedná o herní server hry AgeOfWars");
			    alert.setContentText("Klient je odpojen od serveru");
			    alert.showAndWait();
			    
			    try {
			application.getTCPclient().disconnect();
		    } catch (Exception e) {
		    }
			}
		    });

		    
		}
	    },
		2500
	    );

	}
    }

    @Override
    public void processMessage(String value) {
	try {
	    String data[] = value.split(";");
	    switch (data[0]) { // o jaky prikaz jde?
		case "hi-s": // server se mnou navázal komunikaci
		    handShake = true;
		    System.err.println("handshake: " + handShake);
		    getRooms();
		    break;
		case "lobby": // známý příkaz -> výpis místností
		    list_lobby(data);
		    break;
		case "users":
		    try {
			textArea.appendText(":::> Pocet klientu na serveru je: " + data[1] + "\n");
		    } catch (Exception e) {
			textArea.appendText("Server ma problem pri odesilani poctu klientu");
		    }
		    break;
		case "game":

		    Platform.runLater(() -> {
			start_game.setDisable(false);
		    });

		    if (application.getContainer().isInit() == false) {

			if (data.length == 7) {
			    application.getContainer().setGameSeed(data[1]);

			    if (Integer.parseInt(data[2]) == 1) {
				application.getContainer().setLeader(true);
			    } else {
				application.getContainer().setLeader(false);
			    }

			    application.getContainer().setLeftName(data[3]);
			    application.getContainer().setRightName(data[4]);
			    try {
				int leftTeam = Integer.parseInt(data[5]);
				if (leftTeam < 0) {
				    leftTeam = 1;
				}
				application.getContainer().setLeftTeam(leftTeam);

				int rightTeam = Integer.parseInt(data[6]);
				if (rightTeam < 0) {
				    rightTeam = 1;
				}
				application.getContainer().setRightTeam(rightTeam);

				application.getContainer().setInit(true);
			    } catch (Exception e) {
				System.err.println("Chyba v inicializacni zprave. Pokus o prevod znaku na cislo.");
			    }

			    Platform.runLater(() -> {
				seed_name.setText("Seed: " + application.getContainer().getGameSeed());
				leader_name.setText(application.getContainer().getLeftName());
				opponent_name.setText(application.getContainer().getRightName());
				start_game.setDisable(false);
				updateTeamsImages();
			    });

			} else {
			    textArea.appendText("Server odeslal neplatnou zprávu pro inicializaci hry: " + value);
			}
		    }
		    break;
		case "win":
		    application.getContainer().setInit(false);
		    if (application.getContainer().isLeader()) {
			textArea.appendText(":::> Druhy hrac opustil mistnost\n");
			Platform.runLater(() -> {
			    opponent_name.setText("?");
			    start_game.setDisable(true);
			});
		    } else {// jsem oponent... musím z místnosti pryč
			textArea.appendText(":::> Druhy hrac opustil místnost. V mistnosti neni zakladatel, bude zrusena.\n");
			Platform.runLater(() -> {
			    leader_name.setText("?");
			    start_game.setDisable(true);
			    pane_room.setVisible(false);
			    pane_rooms.setVisible(true);
			});
			application.getTCPclient().sendMessage("leave;");
		    }
		    break;
		case "connected":
		    Platform.runLater(() -> {
			pane_rooms.setDisable(false);
			send.setDisable(false);
			pripojit.setDisable(true);
			odpojit.setDisable(false);
			nickname_field.setDisable(true);
			textBoxAdress.setDisable(true);
			port_field.setDisable(true);
			messageText.setDisable(false);
			messageText.setText("");
			connectionStatus.setText("Připojeno");
		    });
		    break;
		case "disconnected":
		    Platform.runLater(() -> {
			pripojit.setDisable(false);
			pane_rooms.setDisable(true);
			pane_room.setVisible(false);
			pane_rooms.setVisible(true);
			send.setDisable(true);
			pripojit.setDisable(false);
			odpojit.setDisable(true);
			nickname_field.setDisable(false);
			textBoxAdress.setDisable(false);
			port_field.setDisable(false);
			messageText.setDisable(true);
			connectionStatus.setText("Odpojeno");
			textArea.setText("");
			if (data.length > 1) {
			    Alert alert = new Alert(AlertType.ERROR);
			    alert.setTitle("Varování");
			    alert.setHeaderText("Server ukončil spojení s vaším klientem, jste odpojen od serveru.");
			    alert.setContentText("Zkuste se za chvíli připojit znovu");
			    alert.showAndWait();
			}
		    });
		    break;
		case "stop":
		    Platform.runLater(() -> {
			start_game.setDisable(true);
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Varování");
			alert.setHeaderText("Protihráč má problémy s internetovým připojením");
			alert.setContentText("Můžete počkat na jeho návrat do hry");
			alert.showAndWait();
		    });
		    // odeslání rejoin stringů
		    try {
			String rejoin = application.getContainer().getMainRejoinString(null, null);
			application.getTCPclient().sendMessage(rejoin);
		    } catch (Exception e) {
			System.err.println("chyba při odesílání rejoin stringu na server");
		    }

		    break;
		case "rejoin_string":
		    application.getContainer().setRejoinString(value);
		    application.getContainer().applyRejoinString();

		    Platform.runLater(() -> {
			Alert alert = new Alert(AlertType.CONFIRMATION);
			alert.setTitle("Nalezena rozehraná hra");
			alert.setHeaderText("Na serveru byla nalezena vaše rozehraná hra.");
			alert.setContentText("Chcete se do této hry znovu připojit?");
			Optional<ButtonType> result;
			result = alert.showAndWait();
			if (result.get() == ButtonType.OK) {
			    // chce se vrátit do rozehrané hry
			    leader_name.setText(application.getContainer().getLeftName());
			    opponent_name.setText(application.getContainer().getRightName());
			    start_game.setDisable(false);

			    try {
				if (this.application.getContainer().isLeader()) {
				    application.getTCPclient().sendMessage("join_l;" + data[4]);
				} else {
				    application.getTCPclient().sendMessage("join_o;" + data[4]);
				    sleep(2);
				}

			    } catch (Exception e) {
				System.err.println("chyba při odesílání potvrzení znovupřipojrní:");
			    }

			    if (application.getContainer().isSwitchToGame()) {
				application.goToGame();
			    }
			} else {
			    // hráč se nechce znovu připojit do hry, dám vědět druhému, že je konec hry
			    try {
				application.getContainer().setRejoinString("");
				application.getTCPclient().sendMessage("norejoin;");
			    } catch (Exception e) {
				System.err.println("chyba při odesílání _norejoin_");
			    }
			}

		    });

		    //  application.getContainer().applyRejoinString();
		    break;
		case "ready":
		    break;
		case "badname":
		    try {
			application.getTCPclient().sendMessage("leave;");
		    } catch (Exception e) {
		    }
		    Platform.runLater(() -> {
			start_game.setDisable(true);
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Varování");
			alert.setHeaderText("Vaše zvolené jméno na serveru již nekdo používá. Změňtě ho a zkuste se připojit znovu.");
			alert.setContentText("");
			alert.showAndWait();
		    });
		    nickname_field.requestFocus();

		    break;
		case "op_ready":
		    if (application.getContainer().isLeader()) {
			application.getContainer().setRightReady(true);
		    } else {
			application.getContainer().setLeftReady(true);
		    }
		    break;
		case "change_team":
		    Platform.runLater(() -> {
			if (application.getContainer().isLeader()) {
			    try {
				this.processMessage("data;měním pravý tým na " + Integer.parseInt(data[1]));
				application.getContainer().setSwitchRightTeam(Integer.parseInt(data[1]));
			    } catch (Exception e) {
				System.err.println("chyba při změně pravého hráče");
			    }
			} else {
			    try {
				application.getContainer().setSwitchLeftTeam(Integer.parseInt(data[1]));
			    } catch (Exception e) {
				System.err.println("chyba při změně pravého hráče");
			    }
			}
			updateTeamsImages();
		    });
		    break;
		case "norejoin":
		    application.getContainer().setInit(false);
		    // druhý hráč se mnou odmítl pokračovat ve hře...
		    Platform.runLater(() -> {
			if (application.getContainer().isLeader()) {
			    // jsem vedoucí a dostávám na jevo, že v mé místnosti je místo pro někoho nového
			    Alert alert = new Alert(AlertType.CONFIRMATION);
			    alert.setTitle("Protihráč odmítl pokračovat");
			    alert.setHeaderText("Váš protihráč se znovu připojil na server, ale odmítl pokračovat va vaší rozehrané hře.\nServer udělal ve vaší místnosti místo pro nového hráče.");
			    ButtonType buttonTypeOne = new ButtonType("Rozumím");
			    alert.getButtonTypes().setAll(buttonTypeOne);
			    Optional<ButtonType> result = alert.showAndWait();
			    if (result.get() == buttonTypeOne) {
				leader_name.setText(application.getContainer().getLeftName());
				opponent_name.setText("?");
				start_game.setDisable(true);
			    } else {
				leader_name.setText(application.getContainer().getLeftName());
				opponent_name.setText("?");
				start_game.setDisable(true);
			    }
			} else {
			    // jsem opponent a dostávám na jevo, že místnost se ruší
			    Alert alert = new Alert(AlertType.CONFIRMATION);
			    alert.setTitle("Protihráč odmítl pokračovat");
			    alert.setHeaderText("Váš protihráč (zakladatel místnosti) se znovu připojil na server, ale odmítl pokračovat va vaší rozehrané hře.\nServer tuto místnost zrušil.");
			    ButtonType buttonTypeOne = new ButtonType("Rozumím");
			    alert.getButtonTypes().setAll(buttonTypeOne);
			    Optional<ButtonType> result = alert.showAndWait();
			    if (result.get() == buttonTypeOne) {
				leader_name.setText("?");
				opponent_name.setText("?");
				start_game.setDisable(true);
			    } else {
				leader_name.setText("?");
				opponent_name.setText("?");
				start_game.setDisable(true);
			    }
			    try {
				application.getTCPclient().sendMessage("leave;");
			    } catch (Exception e) {
			    }
			}
		    });// konec platform
		    break;
		case "youarein":
		    application.getContainer().setRoom(Integer.parseInt(data[1]));
		    Platform.runLater(() -> {
			label_numberRoom.setText("#" + application.getContainer().getRoom());
		    });
		    try {

		    } catch (Exception e) {
			System.err.println("chyba při nastavování vytvořené místnosti");
		    }
		    break;
		case "yes":
		    break;
		case "no":
		    if (data.length == 2) {
			switch (data[1]) {
			    case "rooms":
				Platform.runLater(() -> {
				    pane_room.setVisible(false);
				    pane_rooms.setVisible(true);
				});
				this.processMessage("data;Vytvoření nové místnosti bylo zamítnuto. Na serveru je založen maximální počet místností.");
				break;
			    case "no_room":
				this.processMessage("data;Nehraješ v žádné místnosti");
				break;
			    case "notexist":
				Platform.runLater(() -> {
				    pane_room.setVisible(false);
				    pane_rooms.setVisible(true);
				});
				this.processMessage("data;Místnost, do které se chcete připojit již bohužel neexistuje.");
				if (application.getTCPclient().connected()) {
				    getRooms();
				}
				break;
			}

		    }
		    break;
		case "data":
		    //System.err.println("data");
		    if (data.length >= 2) {
			textArea.appendText(":::> " + data[1] + "\n");
		    }
		    break;

		default: // neznámý příkaz
		    System.err.println("Neznámá data: " + Arrays.toString(data));
		    Platform.runLater(() -> {
			try {
			    application.getTCPclient().disconnect();
			} catch (Exception e) {
			}
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Chyba");
			alert.setHeaderText("Server nedodržuje konvenci komunikačního protokolu. Zřejmě se nejedná o herní server hry AgeOfWars");
			alert.setContentText("Klient je odpojen od serveru");
			alert.showAndWait();
		    });

		    break;
	    }// konec switche
	} catch (Exception e) {
	    System.err.println("chyba při zpracování zprávy: " + e.toString());
	}
    }

    private void getRooms() {
	try {
	    try {
		sleep(10);
	    } catch (Exception e) {
	    }

	    application.getTCPclient().sendMessage("list;");
	} catch (IOException ex) {
	    textArea.appendText("Chyba při odesílání zprávy");
	}
    }

    private void initListeners() {

	pripojit.setOnAction((event) -> {
	    try {
		String name = nickname_field.getText().replace(";", "");
		nickname_field.setText(name);
	    } catch (Exception e) {
	    }

	    if (nickname_field.getText() == null || nickname_field.getText().equals("") || nickname_field.getText().isEmpty()) {
		nickname_field.setText("Player");
	    }

	    pripojit.setDisable(true);
	    try {
		int port = Integer.parseInt(port_field.getText());
		if (port > 65535 || port < 1024) {
		    this.processMessage("data;Zadejte prosím platné rozmezí portu: 1024 až 65535");
		} else {
		    this.connectServer();
		}

	    } catch (Exception e) {
		this.processMessage("data;Špatně zadaný port. Zadejte prosím číselnou hodnotu");
		port_field.requestFocus();
	    }
	    pripojit.setDisable(false);
	});

	odpojit.setOnAction((event) -> {
	    try {
		this.disconnectServer();
		items.clear();
		listLobby.setItems(items);
	    } catch (Exception e) {
	    }
	});

	send.setOnAction((event) -> {
	    sendMessage();
	});

	refresh_lobbyButton.setOnAction((event) -> {
	    if (application.getTCPclient().connected()) {
		getRooms();
	    }
	});

	nickname_field.textProperty().addListener(
		(observable, oldValue, newValue) -> {
		    if (newValue.length() > 15) {
			nickname_field.setText(oldValue);
		    }
		}
	);

	change_team_left_down.setOnAction((event) -> {

	    try {
		if (application.getTCPclient().connected()) {
		    application.getContainer().setSwitchLeftTeam(-1);
		    updateTeamsImages();
		    application.getTCPclient().sendMessage("change_team;-1");

		}
	    } catch (Exception e) {
		System.err.println("chyba při změně levého týmu");
	    }
	});

	change_team_left_up.setOnAction((event) -> {
	    try {
		if (application.getTCPclient().connected()) {
		    application.getContainer().setSwitchLeftTeam(1);
		    updateTeamsImages();
		    application.getTCPclient().sendMessage("change_team;1");
		}
	    } catch (Exception e) {
	    }
	});

	change_team_right_down.setOnAction((event) -> {

	    try {
		if (application.getTCPclient().connected()) {
		    application.getContainer().setSwitchRightTeam(-1);
		    updateTeamsImages();
		    application.getTCPclient().sendMessage("change_team;-1");
		}
	    } catch (Exception e) {
	    }
	});

	change_team_right_up.setOnAction((event) -> {
	    try {
		if (application.getTCPclient().connected()) {
		    application.getContainer().setSwitchRightTeam(1);
		    updateTeamsImages();
		    application.getTCPclient().sendMessage("change_team;1");
		}
	    } catch (Exception e) {
	    }
	});

	new_lobbyButton.setOnAction((event) -> {
	    application.getContainer().setLeader(true);
	    if (application.getTCPclient().connected()) {
		try {
		    application.getTCPclient().sendMessage("create;");
		    change_team_left_down.setVisible(true);
		    change_team_left_up.setVisible(true);
		    change_team_right_down.setVisible(false);
		    change_team_right_up.setVisible(false);
		    pane_rooms.setVisible(false);
		    pane_room.setVisible(true);
		    leader_name.setText(application.getContainer().getLeftName());
		    opponent_name.setText("?");

		} catch (IOException ex) {
		    System.err.println("chyba při odesílání příkazu create;");
		}
	    }
	});

	leave_lobbyButton.setOnAction((event) -> {
	    if (application.getTCPclient().connected()) {
		try {
		    application.getTCPclient().sendMessage("leave;");
		    pane_rooms.setVisible(true);
		    pane_room.setVisible(false);
		    getRooms();
		    start_game.setDisable(false);
		} catch (IOException ex) {
		    System.err.println("chyba při odesílání příkazu leave;");
		}
	    }
	});

	join_gameButton.setOnAction((event) -> {
	    application.getContainer().setLeader(false);
	    if (application.getTCPclient().connected()) {
		try {
		    if (listLobby.getSelectionModel().getSelectedItem() != null) {
			if (listLobby.getSelectionModel().getSelectedItem().isFull()) {
			    this.processMessage("data;Místnost je obsazena, nelze se do ni pripojit");
			} else {
			    change_team_left_down.setVisible(false);
			    change_team_left_up.setVisible(false);
			    change_team_right_down.setVisible(true);
			    change_team_right_up.setVisible(true);
			    opponent_name.setText(application.getContainer().getRightName());
			    application.getTCPclient().sendMessage("join_o;" + listLobby.getSelectionModel().getSelectedItem().getIdRoom());
			    application.getContainer().setRoom(listLobby.getSelectionModel().getSelectedItem().getIdRoom());
			    pane_rooms.setVisible(false);
			    pane_room.setVisible(true);
			    label_numberRoom.setText("#" + application.getContainer().getRoom());
			}
		    } else {
			this.processMessage("data;Je nutne vybrat nejakou mistnost");
		    }
		} catch (IOException ex) {
		    System.err.println("chyba při odesílání příkazu join;");
		}
	    }
	});

	start_game.setOnAction((event) -> {
	    application.goToGame();
	    try {
		application.getTCPclient().sendMessage("ready;");
		if (application.getContainer().isLeader()) {
		    application.getContainer().setLeftReady(true);
		} else {
		    application.getContainer().setRightReady(true);
		}
		System.out.println("odeslan příkaz ready");
	    } catch (Exception e) {
		System.err.println("Chyba při odeílání příkazu _ready_");
	    }
	});
    }

    private void updateTeamsImages() {

	left_vbox.getChildren().clear();
	right_vbox.getChildren().clear();

	String[] unitStrings = {"A", "B", "C", "castle"};
	for (int i = 0; i < unitStrings.length; i++) {
	    ImageView viewLeft = new ImageView(new Image(getClass().getResource("/soc/images/sprite/" + Integer.toString(application.getContainer().getLeftTeam()) + "/" + unitStrings[i] + ".png").toExternalForm()));
	    ImageView viewRight = new ImageView(new Image(getClass().getResource("/soc/images/sprite/" + Integer.toString(application.getContainer().getRightTeam()) + "/" + unitStrings[i] + ".png").toExternalForm()));

	    Rectangle2D viewportRect_left;
	    Rectangle2D viewportRect_right;

	    if (i >= unitStrings.length - 1) {//vykreslení hradu
		viewportRect_left = new Rectangle2D(0, 0, 200, 250);
		viewLeft.setViewport(viewportRect_left);
		viewLeft.setFitWidth(60);
		viewLeft.setFitHeight(100);

		viewportRect_right = new Rectangle2D(0, 0, 200, 250);
		viewRight.setViewport(viewportRect_right);
		viewRight.setFitWidth(60);
		viewRight.setFitHeight(100);
	    } else {
		viewportRect_left = new Rectangle2D(96, 0, 96, 96);
		viewLeft.setViewport(viewportRect_left);
		viewLeft.setFitWidth(60);
		viewLeft.setFitHeight(60);

		viewportRect_right = new Rectangle2D(96, 0, 96, 96);
		viewRight.setViewport(viewportRect_right);
		viewRight.setFitWidth(60);
		viewRight.setFitHeight(60);
	    }
	    left_vbox.getChildren().add(viewLeft);
	    right_vbox.getChildren().add(viewRight);
	}
    }

    private void sendMessage() {
	try {
	    application.getTCPclient().sendMessage(messageText.getText());
	} catch (IOException ex) {
	    if (!application.getTCPclient().connected) {
		textArea.appendText("Chyba při odesilani zpravy");
		messageText.requestFocus();
	    } else {
		textArea.appendText("Zprávu nelze odeslat, klient není připojen k serveru");
		messageText.requestFocus();
	    }
	}
	messageText.setText(null);
	messageText.requestFocus();
    }

    public void setApp(Main application) {
	this.application = application;
	updateTeamsImages();
	if (application.getTCPclient() != null) {
	    if (application.getTCPclient().connected) {
		this.processMessage("connected");
	    }
	}
	application.getContainer().cleanUp();
	nickname_field.setText(application.getContainer().getName());
	pane_room.setVisible(false);
	pane_rooms.setVisible(true);
    }

    private void disconnectServer() throws IOException, ClassNotFoundException {
	application.getTCPclient().disconnect();
	application.getTCPclient().stop();

    }

    private void list_lobby(String[] data) {

	this.processMessage("data;Aktualni pocet zalozenych mistnosti na serveru: " + (data.length - 1));
	Platform.runLater(() -> {
	    try {
		items.clear();

		for (int i = 1; i < data.length; i++) {

		    String lobbyData[] = data[i].split("#");
		    //0 - id hry
		    //1- prvni jmeno
		    //2-druhe jmeno

		    String secondName = "";
		    if (lobbyData.length <= 2) {
			secondName = "\t\t";
		    } else {
			secondName = lobbyData[2];
		    }

		    items.add(new LobbyCell(lobbyData[1], secondName, Integer.parseInt(lobbyData[0])));
		    try {
			listLobby.setCellFactory(new Callback<ListView<LobbyCell>, ListCell<LobbyCell>>() {
			    @Override
			    public ListCell<LobbyCell> call(ListView<LobbyCell> arg0) {
				ListCell<LobbyCell> cell = new ListCell<LobbyCell>() {
				    @Override
				    protected void updateItem(LobbyCell lc, boolean bt1) {
					super.updateItem(lc, bt1);
					if (lc != null) {
					    setText("Mistnost " + lc.getIdRoom() + "\t\t[" + lc.getLeftName() + "] vs. [" + lc.getRightName() + "]");
					    Image imgL = new Image(getClass().getResource("/soc/images/" + lc.getImgL()).toExternalForm());
					    ImageView imgview2 = new ImageView(imgL);
					    setGraphic(imgview2);
					}
				    }
				};
				return cell;
			    }
			});
		    } catch (Exception e) {
			System.err.println("Chyba při vytváření položek v listview");
		    }
		    listLobby.setItems(items);
		}
	    } catch (Exception e) {

	    }
	});
    }
}
