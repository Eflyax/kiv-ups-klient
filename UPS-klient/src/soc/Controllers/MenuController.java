package soc.Controllers;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.scene.control.Button;

import soc.Main;

/**
 *
 * @author Eflyax
 */
public class MenuController extends Controller {

    private Main application;
    @FXML
    private Button novaHra;
    @FXML
    private Button ukoncitHru;

    public void setApp(Main application) {
	this.application = application;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
	initListeners();
    }

    private void initListeners() {
	ukoncitHru.setOnAction((event) -> {
	    System.exit(0);
	});

	novaHra.setOnAction((event) -> {
	    application.goToLobby();
	});
    }

    @Override
    public void processMessage(String data) {

    }

}
