/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package soc.Controllers;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.Timer;
import java.util.TimerTask;
import javafx.application.Platform;
import soc.Main;

/**
 *
 * @author Eflyax
 */
public class TCPclient extends Thread {

    Socket skt;
    BufferedReader in;
    BufferedWriter bw;
    private Main application;
    private String server;
    private int port;
    boolean connected = false;
    private int repeat = 3;
    boolean isRunning = false;
    Timer heathBeater;
    int heartBeatFail;

    public TCPclient(Main mainApp) {
	this.application = mainApp;
	heathBeater = new Timer();
	heartBeatFail = 0;
    }

    TimerTask myTask = new TimerTask() {
	@Override
	public void run() {

	    try {
		sendMessage("hbeat;");
		heartBeatFail = 0;
	    } catch (Exception e) {
		heartBeatFail++;
		if (heartBeatFail < 2) {
		    Platform.runLater(() -> {
			application.processMessage("disconnected");
		    });
		    try {
			if (connected) {
			    disconnect();
			    application.processMessage("data;Spojeni se serverem je problematicke, klient odpojen");

			    connected = false;
			}
		    } catch (IOException ex) {
			System.err.println("Chyba při odpojování");
		    }
		}
	    }
	}
    };

    public void initConnectionData(String server, int port) {
	this.server = server;
	this.port = port;
    }

    public void disconnect() throws IOException {
	application.processMessage("disconnected");
	connected = false;
	skt.shutdownOutput();
	skt.shutdownInput();
	skt.close();
    }

    @Override
    public void run() {
	isRunning = true;
	for (int i = 0; i < this.repeat; i++) {
	    try {
		this.application.processMessage("data;Pripojuji se k serveru...");
		skt = new Socket(this.server, this.port);
		in = new BufferedReader(new InputStreamReader(skt.getInputStream()));
		bw = new BufferedWriter(new OutputStreamWriter(skt.getOutputStream()));
		connected = true;
		break;
	    } catch (IOException ex) {
		this.application.processMessage("data;Server neodpovida. Pokusime se "+ (repeat - i)  +"x pripojit... ");
		connected = false;
	    }
	    try {
		sleep(1000);
	    } catch (InterruptedException ex) {
	    }
	    
	}
		
	if (connected) {
	    this.application.processMessage("data;Pripojeni bylo uspesne");
	    Platform.runLater(() -> {
		application.processMessage("connected");
	    });
	    try {
		this.sendMessage("hi-c;" + this.application.getContainer().getLeftName());
	    } catch (Exception e) {
	    }

	    try {
		this.receiveMessage();
	    } catch (IOException ex) {

	    }
	    isRunning = false;
	    this.application.processMessage("disconnected;lost");
	    try {
		this.disconnect();
	    } catch (IOException ex) {

	    }
	}else{
	 this.application.processMessage("disconnected");  
	}
    }

    public void receiveMessage() throws IOException {

	heathBeater.schedule(myTask, 1000, 1000);
	//System.out.println("Přijímám od serveru...");
	while (!in.ready()) {
	}
	while (connected) {
	    try {
		String received = in.readLine();
		System.out.println("##>" + received);
		if (!received.equals("")) {
		    this.application.processMessage(received);
		}
	    } catch (Exception e) {
		connected = false;
	    }
	}
    }

    public void sendMessage(String message) throws IOException {
	bw.write(message);
	bw.flush();
    }

    public boolean connected() {
	return this.connected;
    }
}
