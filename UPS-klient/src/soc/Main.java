/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package soc;

import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.fxml.JavaFXBuilderFactory;
import soc.Controllers.LobbyController;
import soc.Controllers.MenuController;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import soc.Controllers.Controller;
import soc.Controllers.GameController;
import soc.Controllers.TCPclient;

/**
 *
 * @author Eflyax
 */
public class Main extends Application {

    private Stage stage;
    private Controller activeController;
    TCPclient tcpClient;
    private Container container;
    int cas;

    @Override
    public void start(Stage primaryStage) throws Exception {
        this.container = new Container();
        cas = 0;

        try {
            stage = primaryStage;
            ///goToGame();
           goToMenu();
          // goToLobby();
            primaryStage.show();
        } catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Application.launch(Main.class, (java.lang.String[]) null);
    }

    public TCPclient getTCPclient() {
        return this.tcpClient;
    }

    public TCPclient setNewTCP() {
        this.tcpClient = new TCPclient(this);
        return this.tcpClient;
    }

    @Override
    public void stop() {
        System.exit(0);
    }

    public Container getContainer() {
        return container;
    }

    public void goToGame() {
        try {
            GameController game = (GameController) replaceSceneContent("GUI/Game.fxml");
            activeController = game;
            game.setApp(this);
        } catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void goToLobby() {
        try {
            LobbyController lobby = (LobbyController) replaceSceneContent("GUI/Lobby.fxml");
            activeController = lobby;
            lobby.setApp(this);
        } catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void goToMenu() {
        try {
            MenuController menu = (MenuController) replaceSceneContent("GUI/Menu.fxml");
            activeController = menu;
            menu.setApp(this);
        } catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private Initializable replaceSceneContent(String fxml) throws Exception {
        FXMLLoader loader = new FXMLLoader();
        InputStream in = Main.class.getResourceAsStream(fxml);
        loader.setBuilderFactory(new JavaFXBuilderFactory());
        loader.setLocation(Main.class.getResource(fxml));
        AnchorPane page;
        try {
            page = (AnchorPane) loader.load(in);
        } finally {
            in.close();
        }
        stage.setTitle("Age of Wars - Online");
        stage.setMinWidth(constants.STAGE_WIDTH);
        stage.setMinHeight(constants.STAGE_HEIGHT);
        stage.setMaxWidth(constants.STAGE_WIDTH);
        stage.setMaxHeight(constants.STAGE_HEIGHT);

        Scene scene = new Scene(page, constants.STAGE_WIDTH, constants.STAGE_HEIGHT);
        stage.setMaxWidth(constants.STAGE_WIDTH);
        stage.setMaxHeight(constants.STAGE_HEIGHT);
        stage.setMinWidth(constants.STAGE_WIDTH);
        stage.setMinHeight(constants.STAGE_HEIGHT);
        stage.setScene(scene);
        stage.sizeToScene();
        return (Initializable) loader.getController();
    }

    public void processMessage(String readLine) {
        activeController.processMessage(readLine);
    }
}
