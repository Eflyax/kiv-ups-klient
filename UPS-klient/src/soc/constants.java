
package soc;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;

/**
 *
 * @author Eflyax
 */
public class constants {
    public static final int MAP_WIDTH = 1111; 
    public static final int STAGE_WIDTH = 1111; 
    public static final int STAGE_HEIGHT = 640; 
    public static final int CANVAS_HEIGHT = 500;
    
    public enum Category{ A, B, C };
	
	public static Category chatToCategory(String value) {
		switch(value){
			case "A":
			case "a": return Category.A; 
			case "B":
			case "b": return Category.B; 
			case "C":
			case "c": return Category.C;
			default:  return Category.A; 
		}
	}
        
    public static String getMD5(String input) {
        byte[] source;
        try {
            //Get byte according by specified coding.
            source = input.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            source = input.getBytes();
        }
        String result = null;
        char hexDigits[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(source);
            //The result should be one 128 integer
            byte temp[] = md.digest();
            char str[] = new char[16 * 2];
            int k = 0;
            for (int i = 0; i < 16; i++) {
                byte byte0 = temp[i];
                str[k++] = hexDigits[byte0 >>> 4 & 0xf];
                str[k++] = hexDigits[byte0 & 0xf];
            }
            result = new String(str);
        } catch (Exception e) {

        }
       // System.out.println(result);
        return result;
    }
      
    public static int[] seedToParametres(String seed) {
        
        String hashSeed = getMD5(seed);
        int[] asciiValues = new int[5];
       
        int blockIndex = 0; 
        for(int x = 0; x < 25; x+=5){
            for(int y = 0; y <= 5; y++){
                asciiValues[blockIndex] += hashSeed.charAt(x+y);
            }
            blockIndex++;
        }       
        asciiValues[0] = asciiValues[0] % 12;   //sin
        asciiValues[1] = asciiValues[1] % 2;    //cos
        asciiValues[2] = asciiValues[2] % 14;   //amplsin
        asciiValues[3] = asciiValues[3] % 20;   //amplcos
        asciiValues[4] = asciiValues[4] % 9;   //noise
        return asciiValues;
    }
    
 
    public static double[] parametresToSeed(double width, double sinuses, double cosinuses, double amplsin, double amplcos, double noise) {
       return null;
    }
    
    
}